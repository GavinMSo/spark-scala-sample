/**
 * @author Gavin So
 * 
 * Solution to the following problem:
 * "Parse the data in two files to check how many asset impressions, views and
 * clicks are present in both the files for each page view id. The output file 
 * should have four columns: page view id, asset impressions, views and clicks"
 * */

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import java.io._
import java.nio.file.{Paths, Files}

object SparkAssess {
  val conf = new SparkConf()
   .setMaster("local[*]")
   .setAppName("SparkAssess")
  
  val sc = new SparkContext(conf) 
  //val fw = new FileWriter(new File("solution.txt" ))
  
  def main(args: Array[String]): Unit = {
    val assets_path = "assets_2014-01-20_00_domU-12-31-39-01-A1-34"    
    val adevents_path = "ad-events_2014-01-20_00_domU-12-31-39-01-A1-34"    
    
    val PS = new ProblemSolver(assets_path, adevents_path)
    val solution = PS.solve(sc)
    if (!Files.exists(Paths.get("solution.txt"))) 
      solution.saveAsTextFile("solution.txt")
    
    sc.stop()  
      
    /*val str_format = "%-40s %-9s %-9s %-9sn";
    
    fw.write(String.format(str_format, "page view id", "impressions", "views", 
        "clicks"))
    for (r <- solution){
      fw.write(String.format(str_format, r._1.toString(), r._2._1.toString(), 
          r._2._2.toString(), r._2._3.toString()))
    }
    fw.flush()
    fw.close()*/
  }
}
