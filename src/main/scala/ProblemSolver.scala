import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD;

/**
 * @author Gavin So
 * 
 * This class represents an instance of a solution to the 
 * assets_ and ad-events_ file queries
 * */

class ProblemSolver(assets_path: String, adevents_path:String) 
  extends java.io.Serializable {
  /**
   * Solves problem for an ad-event_ and an assets_ file
   * 
   * @param sc	SparkContext that cannot be serialized
   * 
   * @return RDD solution to problem
   * */
  def solve(sc: SparkContext): RDD[(String, (Int, Int, Int))] = {    
    val result = solveAssets(sc, assets_path)
      .leftOuterJoin(solveAdEvents(sc, adevents_path))
      .map(v => mapEvents(v))
      .reduceByKey((a, b) => reduceEvents(a, b)).cache()
      
    return (result)
  }
  
  /**
   * "Map" function to create room for view and click instance counters
   * 
   * @param tup		tuple to be expanded
   * 
   * @return tuple with additional element
   */
  def mapEvents(tup: (String,(Int, Option[String]))): 
      (String, (Int, Int, Int)) = {
    
    var views: Int = 0
    var clicks: Int = 0
    
    val unOp = unOption(tup._2._2)
    if (unOp == "view") views = views + 1
    if (unOp == "click") clicks = clicks + 1
    
    (tup._1, (tup._2._1, views, clicks))
  }
  
  /**
   * "Reduce" function that finalizes counts of asset impressions, views, and 
   * clicks.
   * 
   * @param tup1	tuple to reduce
   * @param tup2	other tuple to reduce
   * 
   * @return reduced tuple
   * */
  def reduceEvents(tup1: (Int, Int, Int), tup2: (Int, Int, Int)) = {
    var v1 = tup1._1 //existing agg value from access_
    var v2 = tup1._2 + tup2._2 //agg views
    var v3 = tup1._3 + tup2._3 //agg clicks
    
    (v1, v2, v3)
  } 
  
  /**
   * (pv key, #pv instances) attained for assets_ file.
   * 
   * @param sc		SparkContext
   * @param path	path of the file
   * 
   * @return RDD with (pv key, #pv instances)
   * */
  def solveAssets(sc: SparkContext, path: String): RDD[(String, Int)] = {
    val assetsDF = sc.textFile(path)
    
    val as_suf_regex = (s1: String) => ("\",\"".r).split(s1)
    val as_pre_regex = (s2: String) => ("\"pv\":\"".r).split(s2)
    
    val results = assetsDF
      .map(s => trimString(s, as_pre_regex, as_suf_regex))
      .map(s => (s, 1)) //create tuples to count num instances
      .reduceByKey((a,b) => a + b).cache()
    
    //for (r <- results) println(r)   
    return (results)
  }

  /**
   * (pvid, event) attained for an ad-event_ file. Discards non-clicks and 
   *  non-views.
   * 
   * @param	sc		SparkContext
   * @param path	path of file
   * 
   * @return RDD with (pvid, event)
   * */
  def solveAdEvents(sc: SparkContext, path: String): RDD[(String, String)] = {
    val adeventsDF = sc.textFile(path)
    
    //regexes for "e:"
    val as_suf_regex1 = (s1: String) => ("\",\"".r).split(s1)
    val as_pre_regex1 = (s2: String) => ("\"e\":\"".r).split(s2)
    
    //regexes for "pv:"
    val as_suf_regex2 = (s3: String) => ("\"(,|})".r).split(s3)
    val as_pre_regex2 = (s4: String) => ("\"pv\":\"".r).split(s4)

    //function to filter "e:" for views and clicks
    def eFilterFunc(str: (String, String)): Boolean = {
      (str._2 == "view" || str._2 == "click")
    }
    
    val results = adeventsDF
      .map(s => (trimString(s, as_pre_regex2, as_suf_regex2),
          trimString(s, as_pre_regex1, as_suf_regex1)))
      .filter(eFilterFunc).cache()

    //for (s <- results) println(s) 
    return (results) 
  }
  
  /**
   * Trims a prefix and suffix from substring
   * 
   * @param str		string to work on
   * @param pre 	function that splits by prefix 
   * @param suf		function that splits by suffix
   * 
   * @return substring with prefix and suffix trimmed
   * */
  def trimString(str: String, 
      pre: String => Array[String], 
      suf: String => Array[String]) : String = {
    val prefix = pre(str)
    if (prefix.size > 1){ //check if split successful
      return suf(prefix(1))(0)
    }
    else return "None"
    //suf(pre(str)(1))(0)
  }
  
  /**
   * Extracts string value from Option
   * 
   * @param x	option to be extracted
   * 
   * @return string value of option
   * */
  def unOption(x: Option[String]) = x match {
    case Some(s) => s
    case None => None
  }  
}