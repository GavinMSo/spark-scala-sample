Summary:
Refer to instructions.pdf for the assignment instructions.

Installation:
The program has been packaged by SBT. Available options to run are...

1/ Install SBT and type "sbt run" in the project directory.
2/ Import SBT project into Eclipse and run from Eclipse.

Outputs:
Program outputs are in the "output.txt" folder. 
